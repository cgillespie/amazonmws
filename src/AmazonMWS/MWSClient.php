<?php

namespace AmazonMWS;

class MWSClient {

    const PRODUCT_DATA = '_POST_PRODUCT_DATA';
    const INVENTORY_DATA = '_POST_INVENTORY_AVAILABILITY_DATA_';
    const PRICING_DATA = '_POST_PRODUCT_PRICING_DATA_';
    const ORDER_ACKNOWLEDGEMENT = '_POST_ORDER_ACKNOWLEDGEMENT_DATA_';
    const ORDER_FULFILLMENT = '_POST_FLAT_FILE_FULFILLMENT_DATA_';

    protected $seller;
    protected $endpoint;
    protected $url;
    protected $apiSection;
    protected $verbose;

    public function __construct(SellerDetails $sellerDetails) 
    {
        $this->setSeller($sellerDetails);
        $this->setVerbose(1);
    }

    private function _signParameters(array $parameters, $key, $sort=TRUE) 
    {        
        $data = "POST\n" . $this->getUrl() . "\n" ;    
        $uri = $this->getEndpoint();       
        $uriencoded = implode("/", array_map(array($this, "_urlencode"), explode("/", $uri)));       
        $data .= $uriencoded;
        $data .= "\n";

        uksort($parameters, 'strcmp');

        $data .= $this->_getParametersAsString($parameters); 
        return base64_encode(hash_hmac('sha256', $data, $key, true));
    }

    private function _getParametersAsString($parameters) 
    {       
        $queryParameters = array();
        foreach ($parameters as $key => $value) {
            $queryParameters[] = $key . '=' . $this->_urlencode($value);
        }
        return implode('&', $queryParameters);
    }

    private function _urlencode($value) 
    {
        return str_replace('%7E', '~', rawurlencode($value));
    }

    function call($method, $arguments = array()) 
    {
        $parameters = array(
            'Action' => $method,
            'AWSAccessKeyId' => $this->getSeller()->getAwsAccessKey(),
            'Timestamp'=> gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()),
            'SignatureVersion' => 2,
            'SignatureMethod' => "HmacSHA256",
        );

        $body = '';

        $parameters = array_merge($parameters, $arguments);

        $parameters['Signature'] = $this->_signParameters($parameters, $this->getSeller()->getAwsSecretKey(), $this->getApiSection() != 'Feeds');

        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL, 
            "https://" . $this->getUrl() . $this->getEndpoint() . '?' . http_build_query($parameters, null, '&')
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, $this->getVerbose() ? 1 : 0);

        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: text/plain',
                'Content-MD5: ' . base64_encode(md5($body, true)),
                'x-amazon-user-agent: ' . 'AmazonJavascriptScratchpad/1.0 (Language=Javascript)',
                'User-Agent: ' . 'AmazonJavascriptScratchpad/1.0 (Language=Javascript)'
            )
        );

        $response = curl_exec($ch);
      
        curl_close($ch);

        if ( !$response ) {
            return false ;
        }

        if ( $method == 'GetReport' ) {
            return $response ;
        }

        $xml = simplexml_load_string($response); 

        $resultNode = $method . 'Result' ;

        if ( empty($xml->$resultNode) ) {
            return false;
        }
  
        return $xml->$resultNode ;       
    }

    /**
     * Gets the value of seller.
     *
     * @return mixed
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * Sets the value of seller.
     *
     * @param mixed $seller the seller
     *
     * @return self
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;

        return $this;
    }

    /**
     * Gets the value of url.
     *
     * @return mixed
     */
    public function getUrl()
    {
        return $this->getUrl;
    }

    /**
     * Sets the value of url.
     *
     * @param mixed $url the url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->getUrl = $url;

        return $this;
    }

    /**
     * Gets the value of endpoint.
     *
     * @return mixed
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * Sets the value of endpoint.
     *
     * @param mixed $endpoint the endpoint
     *
     * @return self
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    /**
     * Gets the value of apiSection.
     *
     * @return mixed
     */
    public function getApiSection()
    {
        return $this->apiSection;
    }

    /**
     * Sets the value of apiSection.
     *
     * @param mixed $apiSection the api section
     *
     * @return self
     */
    public function setApiSection($apiSection)
    {
        $this->apiSection = $apiSection;

        return $this;
    }

    /**
     * Gets the value of verbose.
     *
     * @return mixed
     */
    public function getVerbose()
    {
        return $this->verbose;
    }

    /**
     * Sets the value of verbose.
     *
     * @param mixed $verbose the verbose
     *
     * @return self
     */
    public function setVerbose($verbose)
    {
        $this->verbose = $verbose;

        return $this;
    }
}

?>
