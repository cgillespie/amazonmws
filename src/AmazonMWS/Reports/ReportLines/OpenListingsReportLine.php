<?php

namespace AmazonMWS\Reports\ReportLines;

class OpenListingsReportLine {
    protected $sku;
    protected $ASIN;
    protected $price;
    protected $quantity;
    

    /**
     * Gets the value of sku.
     *
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Sets the value of sku.
     *
     * @param mixed $sku the sku
     *
     * @return self
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Gets the value of ASIN.
     *
     * @return mixed
     */
    public function getASIN()
    {
        return $this->ASIN;
    }

    /**
     * Sets the value of ASIN.
     *
     * @param mixed $ASIN the a s i n
     *
     * @return self
     */
    public function setASIN($ASIN)
    {
        $this->ASIN = $ASIN;

        return $this;
    }

    /**
     * Gets the value of price.
     *
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Sets the value of price.
     *
     * @param mixed $price the price
     *
     * @return self
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Gets the value of quantity.
     *
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Sets the value of quantity.
     *
     * @param mixed $quantity the quantity
     *
     * @return self
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }
}