<?php

namespace AmazonMWS\Reports;
use AmazonMWS\SellerDetails;
use AmazonMWS\Reports\ReportLines\OpenListingsReportLine;

class OpenListingsReport extends ReportBase
{
    const REPORT_TYPE = '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_';
    const LINE_SEPARATOR = "\n";

    function __construct() 
    {
        parent::__construct(OpenListingsReport::REPORT_TYPE);
    }

    public function headerLineValid($headerLine)
    {
        return true;        
    }

    public function reportLineSeparator()
    {
        return OpenListingsReport::LINE_SEPARATOR;
    }

    public function createLineObject($line)
    {
        $data = explode("\t", $line);

        $line = new OpenListingsReportLine();
        $line->setSku($data[0])
            ->setASIN($data[1])
            ->setPrice($data[2])
            ->setQuantity(trim($data[3]));

        return $line;
    }
}