<?php

namespace AmazonMWS\Reports;

use AmazonMWS\MWSClient;
use AmazonMWS\SellerDetails;
use SimpleCSVDiff\SimpleCSVDiff;

abstract class ReportBase {

    protected $seller;
    protected $reportType;
    protected $reportRequestId;
    protected $reportId;
    protected $reportContent;
    protected $data;

    function __construct($reportType)
    {
        $this->setReportType($reportType);
    }

    protected function call($method, $parameters = array())
    {
        $seller = $this->getSeller();

        $additional = array(
            'Merchant' => $seller->getSellerId(),
            'Action' => $method,
            'Version' => '2009-01-01'
        );

        if ( !empty($parameters['ReportType']) ) {
            $additional['MarketplaceIdList.Id.1'] = $seller->getMarketplaceId();
        }

        $mwsClient = new MWSClient($this->getSeller());
        $mwsClient
            ->setEndpoint('/')
            ->setApiSection('REPORTS')
            ->setUrl('mws.amazonservices.co.uk');

        return $mwsClient->call($method, array_merge($parameters, $additional));        
    }

    function requestReport() 
    {
        $request = $this->call('RequestReport', array('ReportType' => $this->reportType));

        if ( !$request ) {
            return false;
        }

        if ( $request->ReportRequestInfo->ReportRequestId ) {
            $this->setReportRequestId((string)$request->ReportRequestInfo->ReportRequestId);
        } else {
            throw new InvalidArgumentException('An issue occurred requesting your report');
        }

        return $this;
    }

    function isReady() 
    {
        $reportRequestId = $this->getReportRequestId();

        if ( empty($reportRequestId) ) {
            return false;
        }

        if ( is_null($this->getReportId()) ) {
            $response = $this->call('GetReportList', array('ReportRequestIdList.Id.1' => $reportRequestId));            
            if ( $response->ReportInfo->ReportId ) {
                $this->setReportId((string)$response->ReportInfo->ReportId);
            }
        }

        return $this->getReportId() ? true : false;
    }

    function getReport() 
    {
        if ( !$this->isReady() ) {
            return false;
        }

        $data = $this->call('GetReport', array('ReportId' => $this->getReportId()));
        $this->setReportContent($data);
        return $data ? true : false;      
    }

    /**
     * Gets the value of reportType.
     *
     * @return mixed
     */
    public function getReportType()
    {
        return $this->reportType;
    }

    /**
     * Gets the value of reportRequestId.
     *
     * @return mixed
     */
    public function getReportRequestId()
    {
        return $this->reportRequestId;
    }

    /**
     * Sets the value of reportRequestId.
     *
     * @param mixed $reportRequestId the report request id
     *
     * @return self
     */
    public function setReportRequestId($reportRequestId)
    {
        $this->reportRequestId = $reportRequestId;

        return $this;
    }

    /**
     * Gets the value of reportId.
     *
     * @return mixed
     */
    protected function getReportId()
    {
        return $this->reportId;
    }

    /**
     * Gets the value of data.
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    public function hasData()
    {
        return $this->data ? true : false;
    }

    /**
     * Sets the value of reportId.
     *
     * @param mixed $reportId the report id
     *
     * @return self
     */
    protected function setReportId($reportId)
    {
        $this->reportId = $reportId;

        return $this;
    }

    /**
     * Sets the value of reportType.
     *
     * @param mixed $reportType the report type
     *
     * @return self
     */
    protected function setReportType($reportType)
    {
        $this->reportType = $reportType;

        return $this;
    }

    /**
     * Gets the value of reportContent.
     *
     * @return mixed
     */
    public function getReportContent()
    {
        if ( empty($this->reportContent) ) {            
            $this->reportContent = array();
            $content = explode($this->reportLineSeparator(), $this->data);

            if ( !$this->headerLineValid($content[0]) ) {
                throw new IllegalArgumentException('The report data is not valid. Illegal header row');
            }
            array_shift($content);

            foreach ( $content as $line ) {
                if ( $line ) {
                    $this->reportContent[] = $this->createLineObject($line);
                }
            }
        }

        return $this->reportContent;
    }

    /**
     * Sets the value of reportContent.
     *
     * @param mixed $reportContent the report content
     *
     * @return self
     */
    public function setReportContent($reportContent)
    {
        $this->data = $reportContent;

        return $this;
    }

    public function getDiffAgainst(ReportBase $report)
    {
        if ( get_class($this) != get_class($report) ) {
            throw new InvalidArgumentException('You cannot compare reports of type \'' . getClass($this) . '\' with reports of type \'' . getClass($report) . '\'');
        }

        return new SimpleCSVDiff($report->getData(), $this->getData(), 0, "\t");
    }

    abstract public function headerLineValid($headerLine);

    abstract public function reportLineSeparator();

    abstract public function createLineObject($line);

    /**
     * Sets the value of data.
     *
     * @param mixed $data the data
     *
     * @return self
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Gets the value of seller.
     *
     * @return mixed
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * Sets the value of seller.
     *
     * @param mixed $seller the seller
     *
     * @return self
     */
    public function setSeller(SellerDetails $seller)
    {
        $this->seller = $seller;

        return $this;
    }
}