<?php

namespace AmazonMWS;

class SellerDetails {

    protected $sellerId = null;
    protected $awsAccessKey = null;
    protected $awsSecretKey = null;
    protected $marketplaceId = null;

    function __construct($attributes = array()) 
    {
        foreach ( $attributes as $k => $v ) {
            $setter = 'set' . ucfirst($k);
            $this->$setter($v);
        }
    }

    /**
     * Gets the value of sellerId.
     *
     * @return mixed
     */
    public function getSellerId()
    {
        return $this->sellerId;
    }

    /**
     * Sets the value of sellerId.
     *
     * @param mixed $sellerId the seller id
     *
     * @return self
     */
    public function setSellerId($sellerId)
    {
        $this->sellerId = $sellerId;

        return $this;
    }

    /**
     * Gets the value of awsAccessKey.
     *
     * @return mixed
     */
    public function getAwsAccessKey()
    {
        return $this->awsAccessKey;
    }

    /**
     * Sets the value of awsAccessKey.
     *
     * @param mixed $awsAccessKey the aws access key
     *
     * @return self
     */
    public function setAwsAccessKey($awsAccessKey)
    {
        $this->awsAccessKey = $awsAccessKey;

        return $this;
    }

    /**
     * Gets the value of awsSecretKey.
     *
     * @return mixed
     */
    public function getAwsSecretKey()
    {
        return $this->awsSecretKey;
    }

    /**
     * Sets the value of awsSecretKey.
     *
     * @param mixed $awsSecretKey the aws secret key
     *
     * @return self
     */
    public function setAwsSecretKey($awsSecretKey)
    {
        $this->awsSecretKey = $awsSecretKey;

        return $this;
    }

    /**
     * Gets the value of marketplaceId.
     *
     * @return mixed
     */
    public function getMarketplaceId()
    {
        return $this->marketplaceId;
    }

    /**
     * Sets the value of marketplaceId.
     *
     * @param mixed $marketplaceId the marketplace id
     *
     * @return self
     */
    public function setMarketplaceId($marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;

        return $this;
    }
}